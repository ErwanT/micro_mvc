<?php
class Config {
    static $debug = 0;
    
    static $databases = array (
        'default' => array(
            'driver' => 'pgsql',
            'host' => 'localhost',
            'port' => '5432',
            'name' => 'example1',
            'user' => 'example1_user',
            'pass' => 'example1',
            'schema' => 'my_app'
        ),
        'data' => array(
            'driver' => 'mysql',
            'host' => 'localhost',
            'port' => '3306',
            'name' => 'example2',
            'user' => 'example2_user',
            'pass' => 'example'
        ),
    );
    
    static $defaultController = 'MyController';
    
    static $defaultAction = 'Index';
}

?>
