<?php
class Router
{
    /**
     * Parse requested url and return formated request Object with controller and action
     * 
     * @param string $url
     * @param \Request $request
     * @return \Request
     */
    public static function parse($url, $request) 
    {
        $get_params = explode("&", $url);
        foreach ($get_params as $param) {
            $param_data = explode("=", $param);
            $params[$param_data[0]] = $param_data[1];
        }
        $request->controller = (isset($params['controller'])) ? $params['controller'] : Config::$defaultController;
        $request->action = (isset($params['action'])) ? $params['action'] : Config::$defaultAction;
        
        unset($params['controller']);
        unset($params['action']);
        $request->params = $params;
        return $request;
    }
    
    /**
     * Url generator for assets
     * 
     * @param string $ressource
     * @return string
     */
    public static function assets_url ($ressource) 
    {
        return SITE_ROOT . '/app/assets/' . $ressource;
    }
    
    /**
     * Route generator
     * 
     * @param string $controller
     * @param qtring $action
     * @param array $params
     * @return string
     */
    public static function route_url ($controller, $action = false, $params = false) 
    {
        $url = SITE_ROOT . DS . 'index.php?' . DS . 'controller=' . $controller;
        
        $url = (!empty($action)) ? $url . '&action=' . $action : $url;
        
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $url = $url . '&' . $key . '=' . $value;
            }
        }
        return $url;
    }
}