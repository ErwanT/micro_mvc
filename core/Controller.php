<?php
class Controller {
    /**
     * Request container (called page and action)
     * @var Object 
     */
    protected $request;
    
    /**
     * 
     * @param /Request $request
     */
    public function __construct($request)
    {
        /*First thing to do: open a session*/
        $this->sec_session_start();
        
        $this->request = $request;
        
		/* YOUR CODE HERE */
    }
    
    /**
     * Starts secure session
     */
    private function sec_session_start()
    {
        $session_name = 'sec_session_id'; // Set a custom session name
        $secure = false; // Set to true if using https.
        $httponly = true; // This stops javascript being able to access the session id. 
 
        ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies. 
        $cookieParams = session_get_cookie_params(); // Gets current cookies params.
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
        session_name($session_name); // Sets the session name to the one set above.
        session_start(); // Start the php session
        session_regenerate_id(); // regenerated the session, delete the old one.  
}
}
