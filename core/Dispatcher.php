<?php

class Dispatcher {
    public  $request;
    
    public function __construct() {
        $this->request = new Request();
        Router::parse($this->request->url, $this->request);
        $controller = $this->loadController();
        $action = $this->request->action . 'Action';
        if (!in_array($action, get_class_methods($controller))) {
            $this->error('Le controlleur ' . $this->request->controller . 'n\' a pa de méthode ' . $this->request->action);
        }
        call_user_func_array(array($controller, $action), $this->request->params);
    }
    
    private function error ($msg) {
        header("HTTP>1.0 404 Not Found");
        $controller = new Controller($this->request);
        
        die();
    }
    
    public function loadController () {
        $name =  ucfirst($this->request->controller) . 'Controller';
        return new $name($this->request);
    }
}

?>
