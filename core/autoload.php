<?php
function core_loader($class)
{
    $file = CORE_ROOT . $class . '.php';
    include_once $file;
}
function controller_loader($class)
{
    $file = APP_ROOT . '/controllers/' . $class . '.php';
    include_once $file;
}
function model_loader($class)
{
    $file = APP_ROOT . '/models/' . $class . '.php';
    include_once $file;
}
function config_loader($class)
{
    $file = APP_ROOT . '/config/' . $class . '.php';
    include_once $file;
}
spl_autoload_register('core_loader');
spl_autoload_register('controller_loader');
spl_autoload_register('model_loader');
spl_autoload_register('config_loader');