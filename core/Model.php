<?php
class Model {
    protected $database = 'default';
    protected $db;
    protected $schema = false;
    protected $table = false;
    protected $table_alias = false;
    protected $primary_key = 'id';
    protected $sql;
    protected $params;

    public function __construct() 
    {
        $this->database = Config::$databases[$this->database];
        
        if ($this->schema === false && isset($this->database['schema'])) {
            $this->schema = $this->database['schema'] . '.';
        }
        
        if ($this->table === false) {
            $this->table =  $this->schema . strtolower(str_replace('Model', '', get_class($this)));
        } else if (isset($this->schema) && strpos($this->schema, $this->table) === false) {
            $this->table =  $this->schema . $this->table;
        }
        
        if ($this->table_alias === false) {
            $this->table_alias = substr(strtolower(get_class($this)), 0, 3);
        }
        
        try {
            $this->db = new PDO(
                    $this->database['driver']. 
                    ':host=' . $this->database['host'] . 
                    ';port=' . $this->database['port'] . 
                    ';dbname=' . $this->database['name'] . 
                    ';user=' . $this->database['user'] . 
                    ';password=' . $this->database['pass']
            );
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        } catch (Exception $e) {
            return $e;
        }
    }
    
    /**
     * Find data using given criterias
     * 
     * @param array $req properties
     * @return array
     */
    public function find($req=false) 
    {
        $this->params = array();
        $fields = isset($req['fields']) ? $req['fields'] : false;
        $this->select($fields)
             ->from();
        if (isset($req['joins'])) {
            $this->join($req['joins']);
        }
        if (isset($req['conditions'])) {
            $this->where($req['conditions']);
        }
        if (isset($req['order'])) {
            $this->orderBy($req['order']);
        }
        if (isset($req['group'])) {
            $this->groupBy($req['group']);
        }
        if (isset($req['limit'])) {
            $offset = isset($req['offset']) ? $req['offset'] : false;
            $this->limit($req['limit'], $offset);
        }
        return $this->execute()->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Retrieve first database data using given criteria
     * 
     * @param array $req properties
     * @return object
     */
    public function findFirst($req) 
    {
        return current($this->find($req));
    }
    
    /**
     * Alias for find, no filter
     * 
     * @return array
     */
    public function findAll() 
    {
        return $this->find();
    }
    
    /**
     * Get elements count from given conditions
     * 
     * @param array $conditions conditions
     * @return integer
     */
    public function findCount($conditions = false)
    {
        $params = array('fields' => 'count(' . $this->primary_key. ') as count');
        if ($conditions) {
            $params['conditions'] = $conditions;
        }
        $result = $this->findFirst($params);
        return $result->count;
    }
    
    /**
     * Update data
     * 
     * @param array $req
     * @return boolean
     * @throws Exception
     */
    public function update($req) 
    {
        $this->params = array();
        $this->sql = 'UPDATE ' . $this->table . ' SET ';
        if (isset($req['set'])) {
            if (is_array($req['set'])) {
                foreach ($req['set'] as $k => $setter) {
                    $this->sql .= ($k > 0) ? ', ' : false;
                    $this->sql .= $setter[0] . ' = ? ';
                    $this->params[] = array('value' => $setter[1], 'data_type' => $setter[2]);
                }
            } else {
                $this->sql .= $req['set'];
            }
        } else {
            throw new Exception('No data to update');
        }
        if (isset($req['conditions'])) {
            $this->where($req['conditions']);
        }
        $this->execute();
        return true;
    }
    
    /**
     * Insert data in table and return insert id
     * 
     * @param array $req
     * @return integer
     * @throws Exception
     */
    public function insert($req) 
    {
        $this->params = array();
        if (isset($req)) {
            $this->sql = 'INSERT INTO ' . $this->table;
            if (is_array($req)) {
                $values = false;
                foreach ($req as $k => $value) {
                    $fields[] = $value[0];
                    $this->params[] = array('value' => $value[1], 'data_type' => $value[2]);
                    $values .= ($k !== 0) ? ', ' : '';
                    $values .= '?';
                }
                $this->sql .= '(' . implode(', ', $fields) . ') VALUES (';
                
                $this->sql .= $values . ')';
            } else {
                $this->sql .= ' VALUES (' . $req . ')';
            }
            $this->execute();
            return $this->db->lastInsertId($this->table . '_' . $this->primary_key . '_seq');
        } else {
            throw new Exception('No data to insert');
        }
    }
    
    /**
     * Delete database elements
     * 
     * @param array $id id of elements to delete
     * @return boolean
     */
    public function delete($id) 
    {
        $this->sql = 'DELETE FROM ' . $this->table . ' ';
        
        if (isset($id)) {
            $this->sql .= 'WHERE id';
            if (is_array($id)) {
                $this->sql .= ' IN (';
                foreach($id as $k => $v) {
                    if ($k > 0) {
                        $this->sql .= ', ';
                    } 
                    $this->sql .= '?';
                }
                $this->sql .= ')';
            } else {
                $this->sql .= ' = ?';
            }
        }
        $prepare = $this->db->prepare($this->sql);
        if (isset($id)) {
            if (is_array($id)) {
                foreach ($id as $k => $v) {
                    $prepare->bindParam($k, $v, PDO::PARAM_INT);
                }
            } else {
                $prepare->bindParam(1, $id, PDO::PARAM_INT);
            }
        }
        if ($prepare->execute()) {
            return true;
        }
        return false;
    }
    
    /**
     * Adds Select clause to the query
     * 
     * @param array $fields Fields to add to the query
     * @return \Model
     */
    public function select($fields = false) 
    {
        $this->sql = 'SELECT *';
        if ($fields) {
            if (is_array($fields)) {
                $this->sql = str_replace("*", implode(", ", $fields), $this->sql);
            } else {
                $this->sql = str_replace("*", $fields, $this->sql);
            }
        }
        return $this;
    }
    
    /**
     * Adds From clause to the query
     * 
     * @param string $table table to add to the query
     * @return \Model
     */
    public function from() 
    {
        $this->sql .= ' FROM ' . $this->table . ' as ' . $this->table_alias;
        return $this;
    }
    
    /**
     * Adds Where clause to the query
     * 
     * @param array $conditions conditions to add to the query
     * @return \Model
     */
    public function where($conditions) 
    {
        $this->sql .= ' WHERE ';
        if (!is_array($conditions)) {
            $this->sql .= $conditions;
        } else {
            foreach($conditions as $k => $cond) {
                if ($k > 0 && !preg_match('#(OR|AND)#', $cond[0])) {
                    $this->sql .= ' AND ';
                } 
                $this->sql .= $cond[0] . ' ?';
                $this->params[] = array('value' => $cond[1], 'data_type' => $cond[2]);
            }
        }
        return $this;
    }
    
    /**
     * Adds tables join to the From clause of the query
     * 
     * @param array $req joins to add to the query
     * @return \Model
     */
    public function join($req) 
    {
        if (is_array($req)) {
            foreach ($req as $k => $join) {
                if (!preg_match('#JOIN#', $join)) {
                    $this->sql .= ' INNER JOIN';
                }
                $this->sql .= ' ' . $join[0] . ' ON (' .$join[1] . ')';
            }
        } else {
            $this->sql .= $req;
        }
        return $this;
    }
    
    /**
     * Adds Order By clause to the query
     * 
     * @param array $orders order fields to add
     * @return \Model
     */
    public function groupBy($orders) 
    {
        $this->sql .= ' GROUP BY ';
        if (is_array($orders)) {
            $this->sql .= implode(', ', $orders);
        } else {
            $this->sql .= $orders;
        }
        return $this;
    }
    
    /**
     * Adds Group By clause to the query
     * 
     * @param type $groups
     * @return \Model
     */
    public function orderBy($groups) 
    {
        $this->sql .= ' ORDER BY ';
        if (is_array($groups)) {
            $this->sql .= implode(', ', $groups);
        } else {
            $this->sql .= $groups;
        }
        return $this;
    }
    
    /**
     * Adds Limit and Offset clause to the query
     * 
     * @param integer $limit limit size
     * @param integer $offset offset size
     * @return \Model
     */
    public function limit($limit, $offset = false) 
    {
        $this->sql .= ' LIMIT ' . $limit;
        if (isset($offset)) {
            $this->sql .= ' OFFSET ' .$offset;
        }
        return $this;
    }
    
    /**
     * Execute sql statement
     * 
     * @return object
     */
    public function execute() 
    {
        if(Config::$debug > 0){
            print_r($this);
        }
        try {
            $prepare = $this->db->prepare($this->sql);
            if (!empty($this->params)) {
                foreach ($this->params as $i => $param) {
                    $prepare->bindParam($i+1, $param['value'], $param['data_type']);
                }
            }
            $prepare->execute();
            return $prepare;
        } catch (PDOException $e) {
            if(Config::$debug > 0){
                echo $e->getMessage();
            }
            throw $e;
        }
        
    }
}