# A simple PHP MVC to use as a boilerplate 

## Description
This little boilerplate implements an MVC architecture with:

* a router
* a base controller
* a base model
* a config file

## Using Micro MVC

* Copy all sources in your web directory.
* Add your databases parameters in app/config/config.php
* Create your custom controllers, models and views
