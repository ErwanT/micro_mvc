<?php
/* Définition des paramètres serveurs */
define('SERVER_ROOT' , '/var/www/myapp/');
define('APP_ROOT', SERVER_ROOT . '/app/');
define('CORE_ROOT', SERVER_ROOT . '/core/');
define('SITE_ROOT' , 'http://mydomain/myapp');

/* Chargement des autoloaders */
require_once CORE_ROOT . 'autoload.php';

if (Config::$debug > 0) {
    ini_set('display_errors', 1);
}

/* Récupération de la page */
new Dispatcher();